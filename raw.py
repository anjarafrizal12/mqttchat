from Tkinter import *;
from ScrolledText import ScrolledText
from os import system, name
import paho.mqtt.client as mqtt
import time
import netifaces as ni


global countChat
global countGroup
global connected
global listTeman
connected = False
countChat = 1.0
countGroup = 1.0

#1 Mia Amelia - 1301164310 Halaman Personal Chat dan feature multiline =================
#=======================================================================================
def PersonalChat():

	def on_connect(client, userdata, flags, rc):
   		print("Connected! rc:", rc)

	def on_message(client, userdata, message):
		if str(message.topic) != publish_top:
			global countChat
			s = message.topic
			s = s.replace(uname, '')
			print(s)
			lblChat.insert(countChat, s + ' : ' + message.payload.decode("utf-8") + '\n')
			print(str(message.topic), str(message.payload.decode("utf-8")))
			print('\n')
			countChat = countChat + 1.0

	def on_subscribe(client, userdata, mid, granted_qos):
		print("Subscribed:", str(mid), str(granted_qos))

	def on_unsubscribe(client, userdata, mid):
		print("Unsubscribed:", str(mid))

	def on_publish(client, userdata, mid):
		print("Publish:", client)

	def on_log(client, userdata, level, buf):
		print("log:", buf)

	def on_disconnect(client, userdata, rc):
		if rc != 0:
			print("Unexpected disconnection.")

	# Set the address of your broker and your port. For a local broker, use the IP
	# address. Otherwise, use the web address.
	broker_address = "192.168.1.199"
	# broker_address = <insert your IP address here>
	port = 1883
	# port = 8883 # port for TLS/SSL

	# Create the MQTT client and set the callback functions you want to use

	#Akhir bagian konfigurasi paho mqtt
	#========================================================================================

	client = mqtt.Client()
	client.on_subscribe = on_subscribe
	client.on_unsubscribe = on_unsubscribe
	client.on_connect = on_connect
	client.on_message = on_message
	time.sleep(1) # Sleep for a beat to ensure things occur in order

	ni.ifaddresses('enp2s0')
	ip = ni.ifaddresses('enp2s0')[ni.AF_INET][0]['addr']

	pm = Tk()
	pm.title('Chat Apps Sister Kelompok 6')
	pm.geometry("500x620")
	pm.resizable(False,False)
	labelJudul = Label(pm, text='Chats Apps Pub - Sub', font=("Helvetica",12))
	labelJudul.pack(pady=5, padx=50)

	friendName = StringVar()
	friendName = subscribe_top
	print(friendName)
	labelJudul = Label(pm, text='Personal Chat : ' + friendName, font=("Helvetica",12))
	labelJudul.place(x=10, y=30)

	labelJudul = Label(pm, text='Your IP Address : ' + ip, font=("Helvetica",12))
	labelJudul.place(x=10, y=50)


	lblChat = ScrolledText(pm, undo=True)
	lblChat.pack(padx=10,pady=50)

	client.username_pw_set(uname, uname)
	client.connect(broker_address, port)
	client.loop_start()
	print(uname+friendName)
	client.subscribe(friendName+uname)

	def send():
		global countChat
		print(publish_top)
		message = etPesan.get()
		lblChat.insert(countChat, 'You : ' + message + '\n')
		client.publish(uname+friendName, message)
		etPesan.delete(0,END)
		etPesan.insert(0,'Masukan Pesan')
		countChat = countChat + 1.0

	def messageClear(event):
		etPesan.delete(0,END)


	pesan = StringVar()

	etPesan = Entry(pm, textvariable=pesan)
	etPesan.insert(0, 'Masukan Pesan')
	etPesan.pack(padx=10,pady=2, fill=X)
	etPesan.bind("<Button>",messageClear)

	bKirim = Button(pm, text="Kirim", command=send)
	bKirim.pack(padx=10,pady=2, fill=X)


	pm.mainloop()

#Akhir Halaman Personal Chat dan MultiLine
#=======================================================================================
#1 Mia Amelia - 1301164310 Halaman Personal Chat dan feature multiline =================
#=======================================================================================


#2 Alan Maulana Ibrahim 1301154175 - Halaman Grup Chat =================================
#=======================================================================================
def GrupChat():

	#Bagian konfigurasi paho mqtt
#========================================================================================

	def on_connect(client, userdata, flags, rc):
   		print("Connected! rc:", rc)

	def on_message(client, userdata, message):
		if str(message.topic) == "group" and uname not in message.payload.decode("utf-8"):
			global countGroup
			s = message.topic
			s = s.replace(uname, '')
			print(s)
			lblChat.insert(countGroup, message.payload.decode("utf-8") + '\n')
			print(str(message.topic), str(message.payload.decode("utf-8")))
			print('\n')
			countGroup = countGroup + 1.0

	def on_subscribe(client, userdata, mid, granted_qos):
		print("Subscribed:", str(mid), str(granted_qos))

	def on_unsubscribe(client, userdata, mid):
		print("Unsubscribed:", str(mid))

	def on_publish(client, userdata, mid):
		print("Publish:", client)

	def on_log(client, userdata, level, buf):
		print("log:", buf)

	def on_disconnect(client, userdata, rc):
		if rc != 0:
			print("Unexpected disconnection.")

	# Set the address of your broker and your port. For a local broker, use the IP
	# address. Otherwise, use the web address.
	broker_address = "192.168.1.199"
	# broker_address = <insert your IP address here>
	port = 1883
	# port = 8883 # port for TLS/SSL

	# Create the MQTT client and set the callback functions you want to use

	#Akhir bagian konfigurasi paho mqtt
	#========================================================================================

	client = mqtt.Client()
	client.on_subscribe = on_subscribe
	client.on_unsubscribe = on_unsubscribe
	client.on_connect = on_connect
	client.on_message = on_message
	time.sleep(1) # Sleep for a beat to ensure things occur in order

	ni.ifaddresses('enp2s0')
	ip = ni.ifaddresses('enp2s0')[ni.AF_INET][0]['addr']

	pm = Tk()
	pm.title('Chat Apps Sister Kelompok 6')
	pm.geometry("500x620")
	pm.resizable(False,False)
	labelJudul = Label(pm, text='Chats Apps Pub - Sub', font=("Helvetica",12))
	labelJudul.pack(pady=5, padx=50)

	labelJudul = Label(pm, text='Grup Chat', font=("Helvetica",12))
	labelJudul.place(x=10, y=30)

	labelJudul = Label(pm, text='Your IP Address : ' + ip, font=("Helvetica",12))
	labelJudul.place(x=10, y=50)


	lblChat = ScrolledText(pm, undo=True)
	lblChat.pack(padx=10,pady=50)

	client.username_pw_set(uname, uname)
	client.connect(broker_address, port)
	client.loop_start()
	client.subscribe("group")

	def send():
		global countGroup
		message = etPesan.get()
		lblChat.insert(countGroup, 'You : ' + message + '\n')
		nMessage = uname+ " : " + message
		client.publish("group", nMessage)
		etPesan.delete(0,END)
		etPesan.insert(0,'Masukan Pesan')
		countGroup = countGroup + 1.0

	def messageClear(event):
		etPesan.delete(0,END)


	pesan = StringVar()

	etPesan = Entry(pm, textvariable=pesan)
	etPesan.insert(0, 'Masukan Pesan')
	etPesan.pack(padx=10,pady=2, fill=X)
	etPesan.bind("<Button>",messageClear)

	bKirim = Button(pm, text="Kirim", command=send)
	bKirim.pack(padx=10,pady=2, fill=X)


	pm.mainloop()

#2 Alan Maulana Ibrahim 1301154175 - Akhir Halaman Grup Chat ===========================
#=======================================================================================

#3 Anjar Afrizal 1301154239	- Feature menampilkan daftar teman yang sedang online ======
# bermain merubah def on_message untuk menampilkan daftar teman ========================
def fungsi_mqtt():
	def on_connect(client, userdata, flags, rc):
		print("Connected! rc:", rc)

	def on_message(client, userdata, message):
		if str(message.topic) == "daftarteman":
			dfTeman = message.payload.decode("utf-8")
			global listTeman
			listTeman = dfTeman.split(",")
			print(listTeman)


	def on_subscribe(client, userdata, mid, granted_qos):
		print("Subscribed:", str(mid), str(granted_qos))

	def on_unsubscribe(client, userdata, mid):
		print("Unsubscribed:", str(mid))

	def on_publish(client, userdata, mid):
		print("Publish:", client)

	def on_log(client, userdata, level, buf):
		print("log:", buf)

	def on_disconnect(client, userdata, rc):
		if rc != 0:
			print("Unexpected disconnection.")

	# Alamat Broker
	global broker_address
	broker_address = "192.168.1.199"
	# Port Broker
	global port
	port = 1883

	# membuat client mqtt
	global client
	client = mqtt.Client()
	client.on_subscribe = on_subscribe
	client.on_unsubscribe = on_unsubscribe
	client.on_connect = on_connect
	client.on_message = on_message
	time.sleep(3)

#3 Anjar Afrizal 1301154239	- Feature menampilkan daftar teman yang sedang online ======
#=======================================================================================

#4 Muhammad Hadiyan Wicaksono 1301154220 Feature status online dan ofline pada saat chat =====================
#=============================================================================================================
def fungsi_mqtt_synack():
	def on_connect(client, userdata, flags, rc):
		print("Connected! rc:", rc)

	def on_message(client, userdata, message):
		if str(message.topic) == "daftarteman":
			dfTeman = message.payload.decode("utf-8")
			global listTeman
			listTeman = dfTeman.split(",")
			print(listTeman)


	def on_subscribe(client, userdata, mid, granted_qos):
		print("Subscribed:", str(mid), str(granted_qos))

	def on_unsubscribe(client, userdata, mid):
		print("Unsubscribed:", str(mid))

	def on_publish(client, userdata, mid):
		print("Publish:", client)

	def on_log(client, userdata, level, buf):
		print("log:", buf)

	def on_disconnect(client, userdata, rc):
		if rc != 0:
			print("Unexpected disconnection.")

	# Alamat Broker
	global broker_address
	broker_address = "192.168.1.199"
	# Port Broker
	global port
	port = 1883

	# membuat client mqtt
	global client
	client = mqtt.Client()
	client.on_subscribe = on_subscribe
	client.on_unsubscribe = on_unsubscribe
	client.on_connect = on_connect
	client.on_message = on_message
	time.sleep(3)

#4 Muhammad Hadiyan Wicaksono 1301154220 akhir Feature status online dan ofline pada saat chat =====================
#=============================================================================================================

#Halaman Utama
#========================================================================================
def MainPage():

	def chat():
		global publish_top
		global pw
		pw = user
		publish_top = user
		print(pw,publish_top)
		global subscribe_top
		subscribe_top = etFriend.get()
		print(subscribe_top)
		etFriend.delete(0,END)
		PersonalChat()

	def unameFriendClear(event):
		etFriend.delete(0,END)

	def group():
		GrupChat()

#3 Anjar Afrizal 1301154239	- Feature menampilkan daftar teman yang sedang online dan halaman utama ===============
#==================================================================================================================

	def refresh():
		fungsi_mqtt()
		client.username_pw_set(uname, uname)
		client.connect(broker_address, port)
		client.loop_start()
		client.subscribe("daftarteman")
		time.sleep(3)
		client.disconnect()
		client.loop_stop()

		countTeman = 1.0
		global listTeman
		global lblChat2
		for x in listTeman:
			lblChat2.insert(countTeman, x + '\n')
			countTeman = countTeman + 1.0



	def quit():
		fungsi_mqtt()
		client.username_pw_set(uname, uname)
		client.connect(broker_address, port)
		client.loop_start()
		client.publish("quit", uname)
		time.sleep(1)
		client.disconnect()
		client.loop_stop()
		main.destroy()

	main = Tk()
	main.title('Chat Apps Sister Kelompok 6')
	main.geometry("300x400")
	main.resizable(False,False)

	main.protocol("WM_DELETE_WINDOW", quit)

	labelJudul = Label(main, text='Chats Apps Pub - Sub', font=("Helvetica",12))
	labelJudul.pack(pady=5, padx=50)
	global uname
	uname = StringVar()
	uname = user
	print(uname)
	labelJudul = Label(main, text='Selamat Datang, ' + uname, font=("Helvetica",12))
	labelJudul.pack(pady=5, padx=50)

	label1 = Label(main, text='Memulai Percakapan Personal', font=("Helvetica",12))
	label1.place(x=10, y=80)

	gLogin = Button(main, text="Refresh Friend", command=refresh)
	gLogin.place(x=10, y=110)

	global lblChat2
	lblChat2 = ScrolledText(main, undo=True, width=20, height=4)
	lblChat2.place(x=10, y=150)

	unameFriend = StringVar()

	etFriend = Entry(main, textvariable=unameFriend)
	etFriend.insert(0, 'Username Teman')
	etFriend.place(x=10, y=230)
	etFriend.bind("<Button>",unameFriendClear)

	bLogin = Button(main, text="Chat", command=chat)
	bLogin.place(x=10, y=260)

	divider = Label(main, text='=============================', font=("Helvetica",12))
	divider.place(x=10, y=300)

	label2 = Label(main, text='Memulai Percakapan Grup', font=("Helvetica",12))
	label2.place(x=10, y=320)

	gLogin = Button(main, text="Join Grup", command=group)
	gLogin.place(x=10, y=350)


	main.mainloop()

#3 Anjar Afrizal 1301154239	- Feature menampilkan daftar teman yang sedang online dan halaman utama ===================
#======================================================================================================================


#5 Ramawaldi Putra - 1301164506 Featur Single Username Login dan halaman Login ========================================
#======================================================================================================================
top = Tk()
top.title('Chat Apps Sister Kelompok 6')
top.geometry("300x200")
top.resizable(False,False)

labelJudul = Label(top, text='Chats Apps Pub - Sub', font=("Helvetica",12))
labelJudul.pack(pady=20, padx=50)

username = StringVar()

def Login():
	global user
	user = etUsername.get()

	fungsi_mqtt()

	global client
	client.username_pw_set(user, user)
	client.connect(broker_address, port)
	client.loop_start()

	client.subscribe("daftarteman")
	time.sleep(3)

# Disconnect and stop the loop!

	client.unsubscribe("daftarteman")

	statLogin = True

	global listTeman
	for x in listTeman:
		if x == user:
			statLogin = False
			break

	if statLogin == True:
		client.publish("friend", user)
		time.sleep(3)
		client.disconnect()
		client.loop_stop()
		top.destroy()
		MainPage()
	else:
		print("gagal login")
		statL.set("Maaf username telah dipakai")

	
statL = StringVar()


def usernameClear(event):
	etUsername.delete(0,END)

etUsername = Entry(top, textvariable=username)
etUsername.insert(0, 'Username')
etUsername.pack(padx=10, pady=10)
etUsername.bind("<Button>",usernameClear)

labelStat = Label(top, textvariable=statL, font=("Helvetica",12))
labelStat.pack(pady=5, padx=5)

bLogin = Button(top, text="Login", command=Login)
bLogin.pack(padx=10, pady=10)



top.mainloop()

#5 Ramawaldi Putra - 1301164506 Akhir Feature Single Username Login dan halaman Login =================================
#======================================================================================================================